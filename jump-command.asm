; je = Jump if Equal
; jne = Jump if Not Equal
; jg = Jump if Greater
; jl = Jump if Less
cmp eax, ebx ; compare eax and ebx, and set flag
je eip+0x10 ; jump [over the following 0x10-1=0xf instructions] if equal